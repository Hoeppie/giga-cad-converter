
# GIGA CAD Converter #

Um das Programm zu testen, einfach das [Converter Jar](https://bitbucket.org/Hoeppie/giga-cad-converter/downloads/converter_1.0.2.jar) runterladen
und mit **java -jar converter_1.0.2.jar** aufrufen.

![start_de](images/start_de.gif)

## Ein GIGA-CAD Hires (1, 4up und 10up), Multicolor Converter ##

Die Bilder müssen aktuell als PRG-Dateien in einem Verzeichnis vorliegen.
Mit den Navigator rechts, einfach den Ordner durch Klicken auswählen und dann in der Mitte
das Bild wählen. 

![picturelist_de](images/picturelist_de.gif)

Es sollte dann eine Vorschau erscheinen. Mit Speichern wird dann im Ordner mit den Namen des Bildes ein PNG, GIF oder JPEG angelegt.
Die aktuelle Version ist 1.0.2.

![selection_de](images/selection_de.gif)

## Einstellungen ##
Im Jar sind ein paar Property-Dateien:

**logging.properties** 
Hier kann das Logging-Verhalten eingestellt werden.
   
**converter.properties** 
Diese Datei enthält sprachunabhängige Einstellungen, wie das zu erstellende
Grafik-Format. Auch findet sich hier, wie die Farben des C64 auf RGB gemappen sind.
Die Farbschema sind DEFAULT (Vice), GODOT und ALIEN.

**converter_XX.properties** 
Die sprachabhängigen Ressourcen. Aktuell DE und EN.

### Das Ergebnis ###

![result_de](images/result_de.gif)

### Hilfe gesucht ###
 * Es wäre schön wenn du weitere Sprachfiles erstellst.
 * Beta-Tester und Anmerkungen sind erwünscht, erbeten.
 * (Rechtschreibkontrolle würde mir auch helfen)  

### Dirk Höpfner ###
 * Ich bin ein Teil des [Commodore 64 Club Berlin](http://berlinc64club.de/).
 * Mit-Organisator der [BCCs](http://csdb.dk/search/?seinsel=all&search=BCC%20Party).
 * Bekannt auch als [Hoeppie](http://csdb.dk/scener/?id=19367)
 * Ersteller des Windows-Ports des [ACME C64 Crossassembler](http://www.emu64.de/acme/)

[English]

# GIGA CAD Converter #
To start the program, download the [converter.jar](https://bitbucket.org/Hoeppie/giga-cad-converter/downloads/converter_1.0.2.jar) and run it with **java -jar converter_1.0.2.jar**.

![start_en](images/start_en.gif)

## It is a CIGA-CAD Hires (1, 4up ,and 10 up), Multicolor Converter ##

The pictures need to be available in one directory as PRG files, extracted from a D64 container. Use the navigator on the left to choose your directory containing your files.

![picturelist_en](images/picturelist_en.gif)

Found files will be listed in the middle. Click on a file to display the preview on the right window. By clicking on the “Save” button, the picture will be saved in the same directory with the same name but different file type of a PNG, GIF or JPEG (as configured in the converter.property file). The current version is 1.0.2.

![selection_en](images/selection_en.gif)

## Configuration ##

There are some property files in the jar-archive:

**logging.properties**
A standard logging configuration file.

**converter.properties**
In this file, the language independent configuration, e.g. the target picture format, can be specified. In addition, the C64 color schema for the mapping of the colors to RGB values are specified here. The available maps are: DEFAULT (Vice), GODOT and ALIEN.

**converter_XX.properties**
This are the language dependent texts for the GUI. Currently available only in DE and EN.

### Result ###

![result_en](images/result_en.gif)

### Help wanted ###
* Additional language files needed.
* Beta testing and feedback wanted adn welcome.
* (Spelling and grammar checks.)

### Dirk Höpfner ###
 * I am a member of [Commodore 64 Club Berlin](http://berlinc64club.de/).
 * An additional organiser of [BCCs](http://csdb.dk/search/?seinsel=all&search=BCC%20Party).
 * Known as [Hoeppie](http://csdb.dk/scener/?id=19367)
 * Supporter of windows port of [ACME C64 Crossassembler](http://www.emu64.de/acme/)
 
### Thank you/Danke ###
[Syshack](https://bitbucket.org/Syscosmos/) Rechtschreibkorrektur/Spelling
