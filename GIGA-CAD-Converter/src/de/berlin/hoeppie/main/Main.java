package de.berlin.hoeppie.main;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import de.berlin.hoeppie.gui.MyWindow;

/*
 * GNU GENERAL PUBLIC LICENSE
 * Version 3, 29 June 2007
 */
/**
 * Main. Init logger and reads properties for strings and messages.
 * 
 * @author hoeppie (hoeppie@gmx.de)
 * 
 */
public class Main {
	/** Logger */
	private static final Logger log = Logger.getLogger(Main.class.getName());
	/** String Resources */
	private static ResourceBundle labels = null;

	static {
		setupLogging();
		try {
			labels = ResourceBundle.getBundle("converter");

			File file = new File("converter.properties");
			if (file.exists()) {
				log.info("File-Path:" + file.getAbsolutePath());
				URL[] urls = new URL[1];
				urls[0] = file.toURI().toURL();
				ClassLoader loader = new URLClassLoader(urls);
				labels = ResourceBundle.getBundle("converter", Locale.getDefault(), loader);
			}
		} catch (MissingResourceException | MalformedURLException mre) {
			log.warning("ResourceBoudle exception:"+mre.getMessage());
		}
	}

	/**
	 * Main entry for starting the program.
	 * 
	 * @param args
	 *            Of the command line.
	 */
	public static void main(String[] args) {
		MyWindow window;
		log.info(Main.class.getName() + "Start(" + getValue("titel") + ").");
		window = new MyWindow();
		window.init();
		log.info(Main.class.getName() + "End.");
	}

	/**
	 * get String from localized resources.
	 * 
	 * @param key
	 *            for search value.
	 * @return value for key or key when no match.
	 */
	public static String getValue(String key) {
		String result = "";
		if (key == null)
			key = "default";
		try {
			result = labels.getString(key);
		} catch (MissingResourceException mre) {
			result = key;
			log.warning("No value found! " + key);
		} catch (NullPointerException npe) {
			result = key;
			log.warning("No value found! " + key);
		}
		return result;
	}

	/**
	 * get String from localized resources.
	 * 
	 * @param key
	 *            for search value.
	 * @return value for key or key when no match.
	 */
	public static int getIntValue(String key) {
		int result = 12;
		if (key == null)
			key = "default";
		try {
			result = Integer.parseInt(labels.getString(key));
		} catch (MissingResourceException mre) {
			log.warning("No value found! " + key);
		} catch (NullPointerException npe) {
			log.warning("No value found! " + key);
		}
		return result;
	}

	/**
	 * Setup Logging.
	 */
	private static void setupLogging() {
		if (System.getProperty("java.util.logging.config.file") == null) {
			final InputStream inputStream = Main.class
					.getResourceAsStream("/logging.properties");
			try {
				LogManager.getLogManager().readConfiguration(inputStream);
			} catch (final IOException e) {
				Logger.getAnonymousLogger().severe(
						"Could not load default logging.properties file");
				Logger.getAnonymousLogger().severe(e.getMessage());
			}
		}
	}
}
