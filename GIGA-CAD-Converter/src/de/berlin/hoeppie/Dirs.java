package de.berlin.hoeppie;

import java.io.File;
import java.io.FilenameFilter;
import java.util.logging.Logger;

/*
 * GNU GENERAL PUBLIC LICENSE
 * Version 3, 29 June 2007
 */
/**
 * Get content of dir and provides a list of content
 * 
 * @author hoeppie (hoeppie@gmx.de)
 */
public class Dirs {
	/** Logger */
	private static final Logger log = Logger.getLogger(Dirs.class.getName());
	/** programs */
	private static final String PRG = ".prg";
	/** 1 up pics */
	private static final String PI = "pi.";
	/** 4 up HiRes */
	private static final String HV = "hv.";
	/** 10 up HiTes */
	private static final String HZ = "hz.";

	private String up01Filter = "(?i:" + PI + "*" + PRG + ")";
	private String up04Filter = "(?i:" + HV + "*" + PRG + ")";
	private String up10Filter = "(?i:" + HZ + "*" + PRG + ")";

	public String up01Prefix = "[ 1Up] ";
	public String up04Prefix = "[ 4Up] ";
	public String up10Prefix = "[10Up] ";
	
	private File fileDir;
	private String[] entries;
	private String[] baseNames;

	/**
	 * reads 1Up pics of GIGA Cad
	 * 
	 * @param path
	 */
	public void readDir1Up(String path) {
		log.info("[" + path + "]start.");
		fileDir = new File(path);
		entries = fileDir.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.matches(up01Filter);
			}
		});
		baseNames = new String[entries.length];
		for (int i = 0; i < entries.length; i++) {
			baseNames[i] = entries[i].substring(3, (entries[i].length() - 4));
		}
		log.info("End.");
	}

	/**
	 * reads 4Up pics of GIGA Cad
	 * 
	 * @param path
	 */
	public void readDir4Up(String path) {
		fileDir = new File(path);
		entries = fileDir.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.matches(up04Filter);
			}
		});
		baseNames = new String[(int) (entries.length / 4)];
		int j = 0;
		for (int i = 0; i < entries.length; i++) {
			String name = entries[i].substring(3, (entries[i].length() - 4));
			if (name.endsWith("4")) {
				baseNames[j++] = name.substring(0, name.length() - 2);
			}
		}
	}
	/**
	 * Reads 10 Up pics of GIGA Cad
	 * @param path
	 */
	public void readDir10Up(String path) {
		fileDir = new File(path);
		entries = fileDir.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.matches(up10Filter);
			}
		});
		baseNames = new String[(int) (entries.length / 10)];
		int j = 0;
		for (int i = 0; i < entries.length; i++) {
			String name = entries[i].substring(3, (entries[i].length() - 4));
			if (name.endsWith("10")) {
				baseNames[j++] = name.substring(0, name.length() - 3);
			}
		}
	}
	/**
	 * reads all Pics of GIGA Cad
	 * @param path
	 */
	public void readDirUp(String path) {
		log.info("[" + path + "]start.");
		String[] names1Up, names4Up, names10Up;
		readDir1Up(path);
		names1Up = new String[baseNames.length];
		for(int i=0;i< baseNames.length;i++){
			names1Up[i] = up01Prefix + baseNames[i];
		}
		readDir4Up(path);
		names4Up = new String[baseNames.length];
		for(int i=0;i< baseNames.length;i++){
			names4Up[i] = up04Prefix + baseNames[i];
		}
		readDir10Up(path);
		names10Up = new String[baseNames.length];
		for(int i=0;i< baseNames.length;i++){
			names10Up[i] = up10Prefix + baseNames[i];
		}
		baseNames = new String[names1Up.length+names4Up.length+names10Up.length];
		for(int i=0;i<names1Up.length;i++){
			baseNames[i] = names1Up[i];
		}
		for(int i=0;i<names4Up.length;i++){
			baseNames[i+names1Up.length] = names4Up[i];
		}
		for(int i=0;i<names10Up.length;i++){
			baseNames[i+names1Up.length+names4Up.length] = names10Up[i];
		}
		log.info("End.");
	}

	public String getPath() {
		return fileDir.getAbsolutePath();
	}

	public void setPath(String path) {
		fileDir = new File(path);
	}

	public String[] getNames() {
		return baseNames;
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < entries.length; i++) {
			result.append(entries[i] + "\n");
		}
		for (int i = 0; i < baseNames.length; i++) {
			result.append("[" + baseNames[i] + "]\n");
		}
		return result.toString();
	}

	// Test

	public String getUp01Prefix() {
		return up01Prefix;
	}

	public String getUp04Prefix() {
		return up04Prefix;
	}

	public String getUp10Prefix() {
		return up10Prefix;
	}

	/**
	 * Main test dirs..
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Dirs handle = new Dirs();
		// handle.readDir(".");
		/*
		handle.readDir1Up("../GIGA-CAD-Libs/01");
		System.out.println(handle.toString());
		handle.readDir4Up("../GIGA-CAD-Libs/04");
		System.out.println(handle.toString());
		handle.readDir10Up("../GIGA-CAD-Libs/10");
		System.out.println(handle.toString());
		*/
		handle.readDirUp("../GIGA-CAD-Libs/all");
		System.out.println(handle.toString());
		System.out.println("PATH:" + handle.getPath());
	}

}
