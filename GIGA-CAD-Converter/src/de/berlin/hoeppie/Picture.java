package de.berlin.hoeppie;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Picture {

	/** Bytes of file */
	byte fileBytes[];
	/** String in Memory */
	String data[] = new String[200];

	/**
	 * Reads 8000 bytes of hire/multi c64 file and drops the first to for
	 * address in memory
	 * 
	 * @param fileName
	 */
	private void readBytes(String fileName) {
		byte read[] = null;
		try {
			read = Files.readAllBytes(Paths.get(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		fileBytes = new byte[8000];
		for (int i = 0; i < 8000; i++) {
			fileBytes[i] = read[i + 2];
		}
	}

	/**
	 * creats a string with 0,1 regarding bits of byte
	 * 
	 * @param byte
	 * @return String with 0 or 1 length 8
	 */
	public static String byteToString(byte b) {
		byte[] masks = { -128, 64, 32, 16, 8, 4, 2, 1 };
		StringBuilder builder = new StringBuilder();
		for (byte m : masks) {
			if ((b & m) == m) {
				builder.append('1');
			} else {
				builder.append('0');
			}
		}
		return builder.toString();
	}

	/**
	 * creates a pic containin 0 and 1 in Memory
	 */
	private void createPic() {
		StringBuffer byte0 = new StringBuffer(), byte1 = new StringBuffer(), byte2 = new StringBuffer(), byte3 = new StringBuffer(), byte4 = new StringBuffer(), byte5 = new StringBuffer(), byte6 = new StringBuffer(), byte7 = new StringBuffer();
		int zelle = 0;
		for (int i = 1; i <= 25; i++) { /* 0-24 */
			for (int j = 1; j <= 40; j++) { /* 0-39 */
				byte0.append(byteToString(fileBytes[zelle * 8 + 0]));
				byte1.append(byteToString(fileBytes[zelle * 8 + 1]));
				byte2.append(byteToString(fileBytes[zelle * 8 + 2]));
				byte3.append(byteToString(fileBytes[zelle * 8 + 3]));
				byte4.append(byteToString(fileBytes[zelle * 8 + 4]));
				byte5.append(byteToString(fileBytes[zelle * 8 + 5]));
				byte6.append(byteToString(fileBytes[zelle * 8 + 6]));
				byte7.append(byteToString(fileBytes[zelle * 8 + 7]));
				zelle++;
			}
			data[(i - 1) * 8 + 0] = byte0.toString();
			data[(i - 1) * 8 + 1] = byte1.toString();
			data[(i - 1) * 8 + 2] = byte2.toString();
			data[(i - 1) * 8 + 3] = byte3.toString();
			data[(i - 1) * 8 + 4] = byte4.toString();
			data[(i - 1) * 8 + 5] = byte5.toString();
			data[(i - 1) * 8 + 6] = byte6.toString();
			data[(i - 1) * 8 + 7] = byte7.toString();
			byte0.setLength(0);
			byte1.setLength(0);
			byte2.setLength(0);
			byte3.setLength(0);
			byte4.setLength(0);
			byte5.setLength(0);
			byte6.setLength(0);
			byte7.setLength(0);
		}
	}

	/**
	 * Read file.
	 * 
	 * @param fileName
	 */
	public void readPic(String fileName) {
		readBytes(fileName);
		createPic();
	}

	public String[] getData(){
		return data;
	}
	/**
	 * Overides toString..
	 * 
	 * @return String of Pic
	 */
	public String toString() {
		StringBuffer result = new StringBuffer();
		for (int i = 1; i < 200; i++) {
			result.append(data[i] + "\n");
		}
		return result.toString();
	}
	
	// Test
	
	/**
	 * Test main.
	 * @param args
	 */
	public static void main(String[] args) {
		Picture handle = new Picture();
		handle.readBytes("../GIGA-CAD-Libs/all/pi.giga.h.prg");
		handle.createPic();
		System.out.println(handle);
	}
}
