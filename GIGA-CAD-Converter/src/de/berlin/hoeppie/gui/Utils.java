package de.berlin.hoeppie.gui;

import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.util.logging.Logger;
/*
 * GNU GENERAL PUBLIC LICENSE
 * Version 3, 29 June 2007
 */
/**
 * Utils for GUI.
 * 
 * @author hoeppie (Email: hoeppie@gmx.de)
 * 
 */
public class Utils {
	/** Logger */
	private static final Logger log = Logger.getLogger(Utils.class.getName());

	/** Toolkit as singelton */
	private static Toolkit tk = null;

	/**
	 * Centers a frame on screen.
	 * 
	 * @param window
	 */
	public static void centerFrame(Window w) {
		log.info("Enter centerFrame("+w+")");
		getToolkit();
		GraphicsConfiguration gc = w.getGraphicsConfiguration();
		Rectangle bounds = gc.getBounds();
		w.setLocation(
				(int) (tk.getScreenSize().getWidth()- w.getWidth()) / 2 + bounds.x,
				(int) (tk.getScreenSize().getHeight() - w.getHeight()) / 2 + bounds.y);
		log.info("Leave centerFrame("+w+")");
	}

	/**
	 * Factory init only once as one instance.
	 */
	private static Toolkit getToolkit() {
		if (tk == null)
			tk = Toolkit.getDefaultToolkit();
		return tk;
	}
}
