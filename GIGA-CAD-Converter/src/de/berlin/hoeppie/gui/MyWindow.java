package de.berlin.hoeppie.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Label;
import java.awt.List;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.logging.Logger;

import javax.swing.ImageIcon;

import de.berlin.hoeppie.C64Color;
import de.berlin.hoeppie.Dirs;
import de.berlin.hoeppie.Picture;
import de.berlin.hoeppie.main.Main;

/*
 * GNU GENERAL PUBLIC LICENSE
 * Version 3, 29 June 2007
 */
/**
 * The main GUI window.
 * 
 * @author hoeppie (hoeppie@gmx.de)
 * 
 */
public class MyWindow extends Frame {
	/** Logger */
	private static final Logger log = Logger
			.getLogger(MyWindow.class.getName());
	/** Internal reference for inner classes. */
	private MyWindow handle = this;
	/** serialVersionUID */
	private static final long serialVersionUID = -4629989420285080721L;
	/** FileDialog */
	Dirs dirs;
	/** names of files in one directory */
	String[] names;

	/** pictures needed to create written file. */
	/** 1 up */
	Picture pic = new Picture();
	/** 4 up pic */
	Picture pic4[] = { new Picture(), new Picture(), new Picture(),
			new Picture() };
	/** 10 up pic */
	Picture pic10[] = { new Picture(), new Picture(), new Picture(),
			new Picture(), new Picture(), new Picture(), new Picture(),
			new Picture(), new Picture(), new Picture() };

	/** Gui components */
	PreView preView;
	PreView preView4;
	PreView preView10;

	/** Save button */
	Button save;

	Panel viewPanel;
	Label viewPanelLabel;
	DirectoryPanel directoryPanel;
	List fileList;
	String actualName;
	String fileName;
	String path;

	/**
	 * Creates UI
	 */
	public void init() {
		log.info("start.");
		setLayout(new FlowLayout());
		// init Model
		dirs = new Dirs();
		path = Main.getValue("path");
		if (path.equalsIgnoreCase("path"))
			path = ".";
		// left directory panel
		directoryPanel = new DirectoryPanel();
		directoryPanel.setPath(path);
		directoryPanel.setCallback(this); // change path here
		// Set Titel
		setTitelString(path);
		setBackground(Color.LIGHT_GRAY);
		add(directoryPanel.getDirectoryPanel());
		add(getFilePanel());
		add(getViewPanel());
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				handle.setVisible(false);
				handle.dispose();
				System.exit(0);
			}
		});
		C64Color.setSchema(Main.getValue("c64schema"));
		ImageIcon ii = new ImageIcon(this.getClass().getResource("/logo.gif"));
		Image image = ii.getImage();
		setIconImage(image);
		MenuBar mb = new MenuBar(); 
		Menu mInfo = new Menu(Main.getValue("info.menu"));
		MenuItem miInfo = new MenuItem(Main.getValue("info.menu"));
		mInfo.add(miInfo);
		miInfo.addActionListener(new ActionListener() {
			InfoDialog infoDialog = new InfoDialog(handle);	
			@Override
			public void actionPerformed(ActionEvent e) {
				infoDialog.setVisible(true);
			}
		});
		mb.setHelpMenu(mInfo);
		setMenuBar(mb);
		pack();
		Utils.centerFrame(this);
		setVisible(true);
		log.info("end.");
	}

	/**
	 * Setup the file panel with list of filtered content.
	 * 
	 * @return panel
	 */
	public Panel getFilePanel() {
		Panel result = new Panel();
		result.setLayout(new BorderLayout());
		result.add(new Label(Main.getValue("files") + " :"), BorderLayout.NORTH);
		fileList = new List(Main.getIntValue("fileEntries")); // up
		names = dirs.getNames();
		// add entires
		if (names.length > 0) {
			for (int i = 0; i < names.length; i++) {
				fileList.add(names[i]);
			}
		}
		fileList.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				actualName = names[(int) e.getItem()];
				fileName = actualName.substring(7, actualName.length());
				setViewPanelLabel(); // set name
				if (actualName.startsWith(dirs.getUp01Prefix())) {
					log.info("actualName[" + actualName + "],fileName["
							+ fileName + "].");
					// read pic
					pic.readPic(dirs.getPath() + File.separator + "pi."
							+ fileName + ".prg");
					// get pic data
					String data[] = pic.getData();
					if (fileName.startsWith("m")) { // 4 colors
						Color col0 = C64Color.getC64(Main.getValue("white"));
						Color col1 = C64Color.getC64(Main
								.getValue("light_grey"));
						Color col2 = C64Color.getC64(Main.getValue("grey"));
						Color col3 = C64Color.getC64(Main.getValue("black"));
						preView.clear(col0); // set background
						for (int i = 0; i < 200; i++) {
							String line = data[i];
							for (int j = 0; j < line.length(); j = j + 2) {

								if (line.charAt(j) == '1') {
									if (line.charAt(j + 1) == '1') {
										preView.setRGBPixel(j, i, col3);
										preView.setRGBPixel(j + 1, i, col3);
									} else {
										preView.setRGBPixel(j, i, col1);
										preView.setRGBPixel(j + 1, i, col1);
									}
								} else {
									if (line.charAt(j + 1) == '1') {
										preView.setRGBPixel(j, i, col2);
										preView.setRGBPixel(j + 1, i, col2);
									}
								}
							}
						}
					} else { // pure Hires
						Color col = C64Color.getC64(Main.getValue("white"));
						preView.clear(col);
						col = C64Color.getC64(Main.getValue("black"));
						for (int i = 0; i < 200; i++) {
							String line = data[i];
							for (int j = 0; j < line.length(); j++) {
								if (line.charAt(j) == '1') {
									preView.setRGBPixel(j, i, col);
								}
							}
						}
					}
				} else if (actualName.startsWith(dirs.getUp04Prefix())) {
					log.info("actualName[" + actualName + "],fileName["
							+ fileName + "].");
					String data[][] = new String[4][];
					Color col = C64Color.getC64(Main.getValue("white"));
					preView.clear(col);
					col = C64Color.getC64(Main.getValue("black"));
					// read 4 pics
					pic4[0].readPic(dirs.getPath() + File.separator + "hv."
							+ fileName + " 1.prg");
					pic4[1].readPic(dirs.getPath() + File.separator + "hv."
							+ fileName + " 2.prg");
					pic4[2].readPic(dirs.getPath() + File.separator + "hv."
							+ fileName + " 3.prg");
					pic4[3].readPic(dirs.getPath() + File.separator + "hv."
							+ fileName + " 4.prg");
					data[0] = pic4[0].getData();
					data[1] = pic4[1].getData();
					data[2] = pic4[2].getData();
					data[3] = pic4[3].getData();
					setPart(data[0], col, 0, 0);
					setPart(data[1], col, 160, 0);
					setPart(data[2], col, 0, 100);
					setPart(data[3], col, 160, 100);
				} else if (actualName.startsWith(dirs.getUp10Prefix())) {
					log.info("actualName[" + actualName + "],fileName["
							+ fileName + "].");
					String data[][] = new String[10][];
					Color col = C64Color.getC64(Main.getValue("white"));
					preView.clear(col);
					col = C64Color.getC64(Main.getValue("black"));
					// read 10 pics
					for (int i = 0; i < 10; i++) {
						pic10[i].readPic(dirs.getPath() + File.separator
								+ "hz." + fileName + " " + (i + 1) + ".prg");
						data[i] = pic10[i].getData();
					}
					setPart10(data[0], col, 100, 0);
					setPart10(data[1], col, 0, 0);
					setPart10(data[2], col, 100, 64);
					setPart10(data[3], col, 0, 64);
					setPart10(data[4], col, 100, 128);
					setPart10(data[5], col, 0, 128);
					setPart10(data[6], col, 100, 192);
					setPart10(data[7], col, 0, 192);
					setPart10(data[8], col, 100, 256);
					setPart10(data[9], col, 0, 256);
				}

			}

			private void setPart(String[] data0, Color col, int x, int y) {
				String line0;
				String line1;
				for (int i = 0; i < 100; i++) {
					line0 = data0[2 * i];
					line1 = data0[2 * i + 1];
					for (int j = 0; j < 160; j++) {
						if (getBlack(line0.charAt(j * 2),
								line0.charAt(j * 2 + 1), line1.charAt(j * 2),
								line1.charAt(j * 2 + 1))) {
							preView.setRGBPixel(j + x, i + y, col);
						}

					}
				}
			}

			private void setPart10(String[] data, Color col, int x, int y) {
				int sx, sy;
				String line0 = "", line1 = "", line2 = "";
				char a0, a1, a2, a3, a4, a5, a6, a7, a8;
				for (int i = 0; i < 100; i++) {
					for (int j = 0; j < 64; j++) {
						sy = (int) (320.0f / 100.0f * i);
						sx = (int) (200.0 / 64.0 * j);
						line0 = data[Math.abs((sx - 1)) % 200];
						line1 = data[(sx) % 200];
						line2 = data[(sx + 1) % 200];
						a0 = line0.charAt(Math.abs((sy - 1)) % 320);
						a1 = line0.charAt((sy) % 320);
						a2 = line0.charAt((sy + 1) % 320);
						a3 = line1.charAt(Math.abs((sy - 1)) % 320);
						a4 = line1.charAt((sy) % 320);
						a5 = line1.charAt((sy + 1) % 320);
						a6 = line2.charAt(Math.abs((sy - 1)) % 320);
						a7 = line2.charAt((sy) % 320);
						a8 = line2.charAt((sy + 1) % 320);
						if (getBlack10(a0, a1, a2, a3, a4, a5, a6, a7, a8)) {
							preView.setRGBPixel(j + y, (100-i) + x, col);
						}
					}
				}
			}
		});
		result.add(fileList, BorderLayout.CENTER);
		return result;
	}

	/**
	 * Creates View Panel
	 * 
	 * @return
	 */
	public Panel getViewPanel() {
		viewPanel = new Panel();
		viewPanel.setLayout(new BorderLayout());
		preView = new PreView();
		preView.init();
		viewPanel.add(preView, BorderLayout.CENTER);
		Panel line = new Panel();
		viewPanelLabel = new Label();
		fileName = Main.getValue("default");
		setViewPanelLabel();
		line.add(viewPanelLabel);
		save = new Button(Main.getValue("save"));
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (actualName.startsWith(dirs.getUp01Prefix())) {
					preView.writeView(path + File.separator + fileName);
				} else if (actualName.startsWith(dirs.getUp04Prefix())) {
					if (preView4 == null) {
						preView4 = new PreView();
						// preView4.init(400, 640);
						preView4.init(640, 400);
						
					}
					Color col = C64Color.getC64(Main.getValue("white"));
					preView4.clear(col);
					col = C64Color.getC64(Main.getValue("black"));
					setPic4(preView4, pic4[0].getData(), col, 0, 0);
					setPic4(preView4, pic4[1].getData(), col, 320, 0);
					setPic4(preView4, pic4[2].getData(), col, 0, 200);
					setPic4(preView4, pic4[3].getData(), col, 320, 200);
					preView4.writeView(path + File.separator + fileName);
				} else if (actualName.startsWith(dirs.getUp10Prefix())) {
					if (preView10 == null) {
						preView10 = new PreView();
						preView10.init(1000, 640);
					}
					Color col = C64Color.getC64(Main.getValue("white"));
					preView10.clear(col);
					col = C64Color.getC64(Main.getValue("black"));
					setPic(preView10, pic10[0].getData(), col, 320, 0);
					setPic(preView10, pic10[1].getData(), col, 0, 0);
					setPic(preView10, pic10[2].getData(), col, 320, 200);
					setPic(preView10, pic10[3].getData(), col, 0, 200);
					setPic(preView10, pic10[4].getData(), col, 320, 400);
					setPic(preView10, pic10[5].getData(), col, 0, 400);
					setPic(preView10, pic10[6].getData(), col, 320, 600);
					setPic(preView10, pic10[7].getData(), col, 0, 600);
					setPic(preView10, pic10[8].getData(), col, 320, 800);
					setPic(preView10, pic10[9].getData(), col, 0, 800);
					preView10.writeView(path + File.separator + fileName);

				}
			}

			private void setPic(PreView view, String data[], Color col, int x,
					int y) {
				for (int i = 0; i < 200; i++) {
					String line = data[i];
					for (int j = 0; j < line.length(); j++) {
						if (line.charAt(j) == '1') {
							view.setRGBPixel(i + y, (line.length()-j) + x, col);
						}
					}
				}
			}
			private void setPic4(PreView view, String data[], Color col, int x,
					int y) {
				for (int i = 0; i < 200; i++) {
					String line = data[i];
					for (int j = 0; j < line.length(); j++) {
						if (line.charAt(j) == '1') {
							//view.setRGBPixel(i + y, j + x, col);
							view.setRGBPixel(j + x,i+ y, col);
						}
					}
				}
			}
		});
		line.add(save);
		viewPanel.add(line, BorderLayout.SOUTH);
		return viewPanel;
	}

	/**
	 * Creates underline of preview.
	 */
	public void setViewPanelLabel() {
		viewPanelLabel.setText(Main.getValue("picture") + ": " + fileName);
		viewPanel.revalidate();
	}

	/**
	 * Callback of change of path
	 * 
	 * @param path
	 */
	public void setTitelString(String path) {
		log.info(path);
		this.path = path;
		setTitle(Main.getValue("titel") + " " + Main.getValue("version") + " "
				+ path);
		dirs.readDirUp(path);
		names = dirs.getNames();
		if (fileList == null) {
			fileList = new List();
		}
		fileList.removeAll(); // drop old
		if (names.length > 0) {
			for (int i = 0; i < names.length; i++) {
				fileList.add(names[i]);
			}
		}
	}

	/**
	 * GetBlack
	 * 
	 * @param a0
	 * @param a1
	 * @param a2
	 * @param a3
	 * @return
	 */
	public boolean getBlack(char a0, char a1, char a2, char a3) {
		int black = a0 + a1 + a2 + a3 - '0' * 4;
		if (black >= 2)
			return true;
		return false;
	}

	/**
	 * GetBlack
	 * 
	 * @param a0
	 *            ,a1,a2,a3,a4,a5,a6,a7,a8,a9
	 * @return
	 */
	public boolean getBlack10(char a0, char a1, char a2, char a3, char a4,
			char a5, char a6, char a7, char a8) {
		int black = a0 + a1 + a2 + a3 - '0' * 4;
		black = black + a4 + a5 + a6 + a7 - '0' * 4;
		black = black + a8 - '0';
		if (black > 1)
			return true;
		return false;
	}
}
