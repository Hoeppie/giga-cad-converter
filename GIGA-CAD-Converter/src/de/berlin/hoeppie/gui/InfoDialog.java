package de.berlin.hoeppie.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Event;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Logger;

import javax.swing.JEditorPane;

import de.berlin.hoeppie.main.Main;
/*
 * GNU GENERAL PUBLIC LICENSE
 * Version 3, 29 June 2007
 */
/**
 * Creates an info dialog.
 * @author hoeppie (hoeppie@gmx.de)
 *
 */
public class InfoDialog extends Dialog {

	/** serialVersionUID */
	private static final long serialVersionUID = 9088910441187969954L;
	/** Standard Logger. */
	private static final Logger log = Logger.getLogger(InfoDialog.class.getName());
	/** Close Button */
	private Button close;
	/** helper for callbacks. */
	private InfoDialog win = this;
	
	/**
	 * Konstruktor
	 * @param parent
	 * @param res
	 */
	public InfoDialog(Frame parent) {
		super(parent, true);
		log.info("start");
		setTitle("Hoeppies "+Main.getValue("titel")+" "+Main.getValue("version"));
		setBackground(Color.LIGHT_GRAY);
		setLayout(new BorderLayout());
		Panel panel = new Panel();
		close = new Button(Main.getValue("info.close"));
		panel.add(close);
		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				log.info("InfoDialog.closeButton.actionPerformed("+e+")");
				win.dispose();
			}
		});
		add("South", panel);
		Panel infoPanel = new Panel();
		JEditorPane editor = new JEditorPane();
		editor.setContentType("text/html");
		editor.setEditable(false);
		editor.setText(Main.getValue("info.text"));
		infoPanel.add(editor);
		add(infoPanel, BorderLayout.CENTER);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				log.info("InfoDialog.windowClosing("+e+")");
				dispose();
			}
		});
		pack();
		Utils.centerFrame(this);
		log.info("end");
	}

	/**
	 * Jeder Event des Dialogs kommt hier vorbei
	 */
	public boolean action(Event evt, Object arg) {
		log.info("InfoDialog action [" + evt + "];");
		if (arg.equals(Main.getValue("info.close"))) {
			dispose();
			return true;
		}
		return false;
	}
}
