package de.berlin.hoeppie.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import de.berlin.hoeppie.main.Main;

/*
 * GNU GENERAL PUBLIC LICENSE
 * Version 3, 29 June 2007
 */
/**
 * PreView GUI component
 * 
 * @author hoeppie (hoeppie@gmx.de)
 * 
 */
public class PreView extends Panel {
	/** Logger */
	private static final Logger log = Logger.getLogger(PreView.class.getName());

	/** serialVersionUID */
	private static final long serialVersionUID = -7849316426406371824L;
	/** Width of window, default 200 */
	private int _width = 200;
	/** Height of Window, default */
	private int _height = 320;
	/** Offset around comtainer */
	private int _offset = 10;
	/** Offscreen image. */
	private BufferedImage _img;
	/** Graphics to offscreen. */
	private Graphics _g;

	/**
	 * Creates default size 320, 200
	 */
	public void init() {
		init(320, 200);
	}

	/**
	 * creates offset image and provides graphic.
	 * 
	 * @param width
	 * @param height
	 */
	public void init(int width, int height) {
		log.info("init(" + width + "," + height + ") start.");
		_width = width;
		_height = height;
		_img = new BufferedImage(_width, _height, BufferedImage.TYPE_INT_RGB);
		_g = _img.getGraphics();
		setLayout(null);
		setSize(_width + _offset, _height + _offset);
		log.info("init() end.");
	}

	/**
	 * paints offset image.
	 */
	public void paint(Graphics g) {
		g.drawImage(_img, _offset / 2, _offset / 2, this);
	}

	/**
	 * Set all to white (clear).
	 */
	public void clear() {
		clear(Color.WHITE);
	}

	/**
	 * Set a background color and clears.
	 * 
	 * @param color
	 *            of background
	 */
	public void clear(Color color) {
		Graphics g = getImgGraphics();
		g.setColor(color);
		g.fillRect(0, 0, _width, _height);
		repaint();
	}

	/**
	 * set a pixel with spezific r,g, and b values (0...255).
	 * 
	 * @param x
	 * @param y
	 * @param r
	 * @param g
	 * @param b
	 */
	public void setRGBPixel(int x, int y, int r, int g, int b) {
		int color = (r << 16) | (g << 8) | b;
		getImgGraphics();
		_img.setRGB(x, y, color);
	}

	/**
	 * Set a pixel in color at x,y
	 * 
	 * @param x
	 * @param y
	 * @param color
	 */
	public void setRGBPixel(int x, int y, Color color) {
		getImgGraphics();
		_img.setRGB(x, y, color.getRGB());
	}

	/**
	 * gets a graphic till it is there
	 * 
	 * @return Graphics
	 */
	public Graphics getImgGraphics() {
		while (_g == null) {
		}
		return _g;
	}

	/**
	 * Write a png off name and addes depending on properties
	 * pictype eg. pictype=png then ".png".
	 * 
	 * @param name
	 */
	public void writeView(String name) {
		String type = Main.getValue("pictype");
		log.info("Name["+name+"];Type["+type+"];");
		try {
			File outputfile = new File(name + "." + type);
			ImageIO.write(_img, type, outputfile);
		} catch (IOException e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
	}
}