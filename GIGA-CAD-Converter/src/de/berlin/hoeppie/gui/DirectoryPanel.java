package de.berlin.hoeppie.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.logging.Logger;

import de.berlin.hoeppie.main.Main;
/*
 * GNU GENERAL PUBLIC LICENSE
 * Version 3, 29 June 2007
 */
/**
 * Navigate through filesystem and show directories
 * to dig in.
 * 
 * @author hoeppie (hoeppie@gmx.de)
 */
public class DirectoryPanel {

	/** Logger */
	private static final Logger log = Logger.getLogger(DirectoryPanel.class
			.getName());

	private Panel handle = null;
	private List dirList;
	private String path = ".";
	private MyWindow callback; 
	
	/**
	 * If one already exit give it back or create one.
	 * @return Panel
	 */
	public Panel getDirectoryPanel() {
		if (handle == null)
			handle = createDirectoryPanel();
		return handle;
	}

	/**
	 * Updates List <b>pre</b> list must exist and path has an value <b>post</b>
	 * list contain directory entries
	 */
	private void updateDirList() {
		if (dirList == null)
			throw new IllegalArgumentException("List is not set.");
		if (path == null)
			throw new IllegalArgumentException("Path is not set.");
		if (path.length() == 0)
			path = File.listRoots()[0].getPath();
		File file = new File(path);
		try {
			path = file.getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String dirs[] = file.list(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return new File(dir + File.separator + name).isDirectory(); // accept
																			// only
																			// directory
			}
		});
		dirList.removeAll(); // drops old contents
		dirList.add(Main.getValue("up")); // up
		for (int i = 0; i < dirs.length; i++) {
			dirList.add(dirs[i]); // new ones
		}
	}

	/**
	 * Creates a panel with list and navigation action.
	 * @return
	 */
	private Panel createDirectoryPanel() {
		Panel directoryPanel = new Panel();
		directoryPanel.setLayout(new BorderLayout());
		directoryPanel.add(new Label(Main.getValue("directory") + " :"),
				BorderLayout.NORTH);
		dirList = new List(Main.getIntValue("dirEntries"));
		updateDirList();
		dirList.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int line = Integer.parseInt(e.getItem().toString());
				String entry = dirList.getItem(line);
				if (line == 0) { // one line up
					log.info("Selected: " + entry);
					File file = new File(path);
					path = file.getParentFile().getAbsolutePath();
					updateDirList();
				} else {
					log.info("Selected: " + entry);
					path = path + File.separator + entry;
					log.info("path: " + path);
				}
				updateDirList();
				if( callback != null){
					callback.setTitelString(path);
				}
			}
		});
		Panel dirPanel = new Panel();
		dirPanel.setLayout(new FlowLayout());
		directoryPanel.add(dirList, BorderLayout.CENTER);
		return directoryPanel;
	}

	/**
	 * set inital path.
	 * 
	 * @param value
	 */
	public void setPath(String value) {
		path = value;
	}

	/**
	 * Get actual directory-path
	 * 
	 * @return path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Set a callback for path changes.
	 * @param callback
	 */
	public void setCallback(MyWindow callback) {
		this.callback = callback;
	}

	// Test 
	static Frame frame;

	public static void main(String[] args) {
		DirectoryPanel handel = new DirectoryPanel();
		frame = new Frame();
		frame.setTitle("Test directory panel");
		frame.add(handel.getDirectoryPanel());
		frame.pack();
		Utils.centerFrame(frame);
		frame.setVisible(true);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				frame.setVisible(false);
				frame.dispose();
			}
		});
	}

}
