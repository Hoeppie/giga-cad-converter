package de.berlin.hoeppie;

import java.awt.Color;
import java.util.logging.Logger;

/*
 * GNU GENERAL PUBLIC LICENSE
 * Version 3, 29 June 2007
 */
/**
 * C64 color
 * 
 * @author hoeppie (hoeppie@gmx.de)
 */
public class C64Color {
	/** Logger */
	private static final Logger log = Logger
			.getLogger(C64Color.class.getName());

	public static enum Schema {
		DEFAULT, GODOT, ALIEN
	}

	public static enum C64Names {
		BLACK, WHITE, RED, CYAN, PUPLE, GREEN, BLUE, YELLOW, ORANGE, BROWN, LIGHT_RED, DARK_GREY, GREY, LIGHT_GREEN, LIGHT_BLUE, LIGHT_GREY
	}

	private static Schema schema = Schema.GODOT;

	private static int[][] colors = {
			{ 0x000000, 0xffffff, 0x68372B, 0x70A4B2, 0x6F3D86, 0x588D43,
					0x352879, 0xB8C76F, 0x6F4F25, 0x433900, 0x9A6759, 0x444444,
					0x6C6C6C, 0x9AD284, 0x6C5EB5, 0x959595 },
			{ 0x000000, 0xffffff, 0x880000, 0xaaffee, 0xcc44cc, 0x00cc55,
					0x0000aa, 0xeeee77, 0xdd8855, 0x664400, 0xff7777, 0x333333,
					0x777777, 0xaaff66, 0x0088ff, 0xbbbbbb },
			{ 0x000000, 0xFFFFFF, 0x744335, 0x7CACBA, 0x7B4890, 0x64974F,
					0x403285, 0xBFCD7A, 0x7B5B2F, 0x4f4500, 0xa37265, 0x505050,
					0x787878, 0xa4d78e, 0x786abd, 0x9f9f9f } };
	
	/**
	 * Gets C64 color following 0..15
	 * @param num
	 * @return
	 */
	public static Color getC64(int num){
		log.info("Number="+num);
		int color = colors[schema.ordinal()][num];
		log.info("Farbe = 0x"+Integer.toHexString(color));
		return new Color(color);
	}

	/**
	 * Gets C64 color with name
	 * @param name
	 * @return
	 */
	public static Color getC64(C64Names name){
		log.info("Name="+name);
		int color = colors[schema.ordinal()][name.ordinal()];
		log.info("Farbe = 0x"+Integer.toHexString(color));
		return new Color(color);
	}

	/**
	 * Gets C64 color with color name
	 * @param name
	 * @return
	 */
	public static Color getC64(String name){
		log.info("Name="+name);
		int color = colors[schema.ordinal()][C64Names.valueOf(name.toUpperCase()).ordinal()];
		log.info("Farbe = 0x"+Integer.toHexString(color));
		return new Color(color);
	}

	/**
	 * Sets a color Schema
	 * @param schema
	 */
	public static void setSchema(Schema _schema){
		log.info(""+_schema);
		schema = _schema;
	}
	/**
	 * Sets a color Schema
	 * @param schema
	 */
	public static void setSchema(String name){
		log.info(name);
		try{
			schema = Schema.valueOf(name);
		}catch(IllegalArgumentException iae){
			log.info(iae.getMessage());
			setSchema(Schema.DEFAULT);
		}
	}
	
	// Test
	public static void main(String[] args) {
		for (int i = 0; i < 16; i++) {
			C64Color.getC64(i);
		}
		C64Color.getC64(C64Names.BLACK);
		C64Color.getC64("WHITE");
	}

}
